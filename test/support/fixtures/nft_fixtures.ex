defmodule GreetGenerators.NFTFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `GreetGenerators.NFT` context.
  """

  @default_erc721 %{
    chain_id: 4,
    description: "Cool NFT project",
    minting_coin_address: "0x0000000000000000000000000000000000000000",
    minting_price: 0,
    mints_per_address: -1,
    name: "NFT Token",
    symbol: "TKN",
    token_uri: "some token_uri",
    total_supply: 42,
    whitelist_signer: "0xee90eeccc941dcb67e175bc17fdd6c260a9eac9a"
  }

  @doc """
  Default ERC721 data.
  """
  def default_erc721, do: @default_erc721

  @doc """
  Generate erc721.
  """
  def erc721_fixture(attrs \\ %{}) do
    {:ok, erc721} =
      attrs
      |> Enum.into(@default_erc721)
      |> GreetGenerators.NFT.create_erc721()

    erc721
  end
end
