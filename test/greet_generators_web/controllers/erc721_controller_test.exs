defmodule GreetGeneratorsApi.ERC721ControllerTest do
  use GreetGeneratorsWeb.ConnCase

  import GreetGenerators.NFTFixtures

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create erc721" do
    test "renders erc721 when data is valid", %{conn: conn} do
      conn = post(conn, Routes.erc721_path(conn, :create), erc721: default_erc721())
      assert %{"contracts" => _contracts} = json_response(conn, 201)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.erc721_path(conn, :create), erc721: default_erc721() |> Map.put(:whitelist_signer, "0x"))
      assert json_response(conn, 422)["errors"] != %{}
    end
  end
end
