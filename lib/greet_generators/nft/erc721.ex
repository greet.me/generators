defmodule GreetGenerators.NFT.ERC721 do
  use Ecto.Schema
  import Ecto.Changeset
  import GreetGenerators.EctoValidators

  @zero_address "0x0000000000000000000000000000000000000000"
  def zero_address, do: @zero_address

  embedded_schema do
    field :name, :string
    field :symbol, :string
    field :description, :string
    field :chain_id, :integer
    field :minting_coin_address, :string, default: @zero_address
    field :minting_price, :integer, default: 0
    field :mints_per_address, :integer, default: -1
    field :token_uri, :string
    field :total_supply, :integer
    field :whitelist_signer, :string, default: @zero_address
    field :contract_name, :string
  end

  @required_fields [
    :name,
    :symbol,
    :description,
    :total_supply,
    :chain_id
  ]

  @optional_fields [
    :mints_per_address,
    :minting_price,
    :minting_coin_address,
    :token_uri,
    :whitelist_signer,
    :contract_name
  ]

  @doc false
  def changeset(erc721, attrs) do
    erc721
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_eth_address([:whitelist_signer, :minting_coin_address])
    |> maybe_set_default_contract_name()
  end

  @doc """
  Provide default camel cased contract name if it's not given.
  """
  def maybe_set_default_contract_name(changeset) do
    case get_field(changeset, :contract_name) do
      nil ->
        name = get_field(changeset, :name)
        contract_name = String.replace(name, " ", "_") |> String.downcase() |> Macro.camelize()
        put_change(changeset, :contract_name, contract_name)

      _ ->
        changeset
    end
  end
end
