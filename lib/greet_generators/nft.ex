defmodule GreetGenerators.NFT do
  @moduledoc """
  The NFT context.
  """

  alias GreetGenerators.NFT.ERC721

  @doc """
  Creates erc721 solidity contract.

  ## Examples

      iex> create_erc721(%{field: value})
      {:ok, ""}

      iex> create_erc721(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_erc721(map()) :: {:ok, String.t()} | {:error, Ecto.Changeset.t()}
  def create_erc721(attrs \\ %{}) do
    %ERC721{}
    |> ERC721.changeset(attrs)
    |> case do
      %{valid?: true} = changeset ->
        erc721 = Ecto.Changeset.apply_changes(changeset)
        render = Phoenix.View.render(GreetGeneratorsApi.ERC721View, "create.sol", erc721: erc721)
        |> Phoenix.HTML.Safe.to_iodata()
        |> :erlang.iolist_to_binary()

        ts = :os.system_time()
        name = "erc721-#{ts}.sol"
        input = System.cwd() <> "/priv/sol/#{name}"
        output = System.cwd() <> "/priv/abi/#{name}"
        File.write!(input, render)
        opts = config!(:solc_opts) ++ ["-o", output, input]
        System.cmd(config!(:solc_compiler), opts)
        json = File.read!(output <> "/combined.json") |> Jason.decode!()
        File.rm_rf!(output)
        File.rm_rf!(input)
        {:ok, json}

      changeset ->
        {:error, changeset}
    end
  end

  @doc """
  Read config option for this module
  """
  def config!(key) do
    Application.get_env(:greet_generators, GreetGenerators.NFT)
    |> Keyword.fetch!(key)
  end
end
