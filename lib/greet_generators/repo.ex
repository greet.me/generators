defmodule GreetGenerators.Repo do
  use Ecto.Repo,
    otp_app: :greet_generators,
    adapter: Ecto.Adapters.Postgres
end
