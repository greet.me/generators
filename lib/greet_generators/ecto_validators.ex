defmodule GreetGenerators.EctoValidators do
  @doc """
  Check if supplied address is a proper Ethereum address.
  """
  @spec validate_eth_address(Ecto.Changeset.t(), [atom()]) :: Ecto.Changeset.t()
  def validate_eth_address(changeset, fields) do
    Enum.reduce(fields, changeset, fn(field, changeset) ->
      value = Ecto.Changeset.get_field(changeset, field)
      case value do
        "0x" <> <<_::bytes-size(40)>> ->
          changeset

        _ ->
          Ecto.Changeset.add_error(changeset, field, "invalid address format")
      end
    end)
  end
end
