defmodule GreetGeneratorsApi.ERC721Controller do
  use GreetGeneratorsApi, :controller

  alias GreetGenerators.NFT

  action_fallback GreetGeneratorsApi.FallbackController

  @doc """
  Generate NFT solidity contract for the supplied data inputs.
  """
  def create(conn, %{"erc721" => erc721_params}) do
    with {:ok, json} <- NFT.create_erc721(erc721_params) do
      conn
      |> put_status(:created)
      |> json(%{data: json})
    end
  end
end
